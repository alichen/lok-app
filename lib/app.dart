import "package:flutter/material.dart";
import "./scene/home.dart";

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.yellowAccent),
        home: HomeScene());
  }
}
