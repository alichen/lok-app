part of bloc;

enum RemarksEvent { Fetch, Refresh }

abstract class RemarksState extends Equatable {
  const RemarksState();

  @override
  List<Object> get props => [];
}

class RemarksEmpty extends RemarksState {
  final List<String> remarks = [];

  RemarksEmpty();

  @override
  List<Object> get props => [remarks];
}

class RemarksLoaded extends RemarksState {
  final List<String> remarks;

  const RemarksLoaded({@required this.remarks}) : assert(remarks != null);

  @override
  List<Object> get props => [remarks];
}

class RemarksError extends RemarksState {}

class RemarksBloc extends Bloc<RemarksEvent, RemarksState> {
  @override
  RemarksState get initialState => RemarksEmpty();

  @override
  Stream<RemarksState> mapEventToState(RemarksEvent event) async* {
    switch (event) {
      case RemarksEvent.Fetch:
        yield* _mapFetchEventToState(event);
        break;
      case RemarksEvent.Fetch:
        yield* _mapFetchEventToState(event);
        break;
      default:
        break;
    }
  }

  Stream<RemarksState> _mapFetchEventToState(RemarksEvent event) async* {
    try {
      final _ret = await fetchRemarks();
      yield RemarksLoaded(remarks: _ret);
    } catch (_) {
      yield RemarksError();
    }
  }
}
