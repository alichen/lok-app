part of bloc;

abstract class StuffsEvent extends Equatable {
  const StuffsEvent();
}

class FetchStuffs extends StuffsEvent {
  final String remark;
  final String path;
  final int p;
  final String q;
  FetchStuffs(
      {@required this.remark, @required this.path, this.p = 1, this.q = ""})
      : assert(remark != null);
  @override
  List<Object> get props => [path];
}

class RefreshStuffs extends StuffsEvent {
  final String remark;
  final String path;
  final String q;
  RefreshStuffs({@required this.remark, @required this.path, this.q = ""})
      : assert(path != null);
  @override
  List<Object> get props => [path];
}

abstract class StuffsState extends Equatable {
  const StuffsState();

  @override
  List<Object> get props => [];
}

class StuffsEmpty extends StuffsState {}

class StuffsLoaded extends StuffsState {
  final PRData data;

  StuffsLoaded({@required this.data}) : assert(data != null);

  @override
  List<Object> get props => [data];
}

class StuffsRefreshed extends StuffsState {
  final PRData data;

  StuffsRefreshed({@required this.data}) : assert(data != null);

  @override
  List<Object> get props => [data];
}

class StuffsError extends StuffsState {}

class StuffsBloc extends Bloc<StuffsEvent, StuffsState> {
  @override
  StuffsState get initialState => StuffsEmpty();

  @override
  Stream<StuffsState> mapEventToState(StuffsEvent event) async* {
    if (event is FetchStuffs) {
      yield* _mapFetchStuffsToState(event);
    } else if (event is RefreshStuffs) {
      yield* _mapRefreshStuffsToState(event);
    }
  }

  Stream<StuffsState> _mapFetchStuffsToState(FetchStuffs event) async* {
    try {
      final _ret = await fetchStuffs(
          p: event.p, q: event.q, remark: event.remark, path: event.path);
      final _rows = _ret["rows"].map<Stuff>((v) => Stuff.fromJson(v)).toList();
      List _next;
      if (state is StuffsEmpty) {
        _next = _rows;
      }
      if (state is StuffsLoaded) {
        final _prev = (state as StuffsLoaded).data.rows;
        _next = _prev..addAll(_rows);
      }
      print("===>next ${_next.length}");
      final _data =
          PRData(total: _ret["count"], count: _next.length, rows: _next);
      yield StuffsLoaded(data: _data);
    } catch (_) {
      yield StuffsError();
    }
  }

  Stream<StuffsState> _mapRefreshStuffsToState(RefreshStuffs event) async* {
    try {
      final _ret = await fetchStuffs(
          p: 1, q: event.q, remark: event.remark, path: event.path);
      final _rows = _ret["rows"].map<Stuff>((v) => Stuff.fromJson(v)).toList();
      List _next;
      if (state is StuffsEmpty) {
        _next = _rows;
      }
      if (state is StuffsLoaded) {
        final _prev = (state as StuffsLoaded).data.rows;
        _next = _prev..addAll(_rows);
      }
      final _data =
          PRData(total: _ret["count"], count: _next.length, rows: _next);
      yield StuffsLoaded(data: _data);
    } catch (_) {
      yield StuffsError();
    }
  }
}
