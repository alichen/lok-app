part of bloc;

abstract class PathsEvent extends Equatable {
  const PathsEvent();
}

class FetchPaths extends PathsEvent {
  final String remark;

  const FetchPaths({@required this.remark}) : assert(remark != null);

  @override
  List<Object> get props => [remark];
}

class RefreshPaths extends PathsEvent {
  final String remark;

  const RefreshPaths({@required this.remark}) : assert(remark != null);

  @override
  List<Object> get props => [remark];
}

abstract class PathsState extends Equatable {
  const PathsState();

  @override
  List<Object> get props => [];
}

class PathsEmpty extends PathsState {
  final List<String> paths = [];

  PathsEmpty();

  @override
  List<Object> get props => [paths];
}

class PathsLoaded extends PathsState {
  final List<String> paths;

  const PathsLoaded({@required this.paths}) : assert(paths != null);

  @override
  List<Object> get props => [paths];
}

class PathsError extends PathsState {}

class PathsBloc extends Bloc<PathsEvent, PathsState> {
  @override
  PathsState get initialState => PathsEmpty();

  @override
  Stream<PathsState> mapEventToState(PathsEvent event) async* {
    if (event is FetchPaths) {
      yield* _mapFetchEventToState(event);
    }
    if (event is RefreshPaths) {
      // yield* _mapFetchEventToState(event);
    }
  }

  Stream<PathsState> _mapFetchEventToState(FetchPaths event) async* {
    try {
      final _ret = await fetchPaths(event.remark);
      yield PathsLoaded(paths: _ret);
    } catch (_) {
      yield PathsError();
    }
  }
}
