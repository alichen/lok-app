import "package:flutter/material.dart";
import "../bloc/bloc.dart";
import "../widget/widget.dart";
import "../model/model.dart";
import "./search.dart";

class StuffsScene extends StatefulWidget {
  final String remark;
  final String path;

  StuffsScene({Key key, @required this.remark, @required this.path})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _StuffsState();
  }
}

class _StuffsState extends State<StuffsScene> {
  final _bloc = new StuffsBloc();

  @override
  void initState() {
    super.initState();
    _fetchStuffs(1);
  }

  void _fetchStuffs(int p) {
    return _bloc
        .add(FetchStuffs(p: p, remark: widget.remark, path: widget.path));
  }

  void _refreshStuffs() {
    return _bloc.add(RefreshStuffs(remark: widget.remark, path: widget.path));
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _renderRow(BuildContext context, List data, int i) {
    final _stuff = data[i] as Stuff;
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 12.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black12))),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(_stuff.name),
                  RichText(
                      text: TextSpan(
                          text: _stuff.updatedAt,
                          style:
                              TextStyle(color: Colors.black45, fontSize: 12.0),
                          children: <TextSpan>[
                        TextSpan(text: '   ' + _stuff.remark),
                        TextSpan(text: '/' + _stuff.path)
                      ]))
                ],
              ),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0.5,
            centerTitle: true,
            title: Text("Stuffs", style: TextStyle(fontSize: 16.0)),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              iconSize: 16.0,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.search),
                  iconSize: 20.0,
                  onPressed: () {
                    _fetchStuffs(2);
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => new SearchScene(remark: widget.remark, path: widget.path)
                    ));
                  })
            ]),
        body: SafeArea(
            bottom: true,
            child: PRList(
              bloc: _bloc,
              fetchRows: _fetchStuffs,
              refresh: _refreshStuffs,
              renderRow: _renderRow,
            )
          ));
  }
}
