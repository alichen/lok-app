import "package:flutter/material.dart";
import "../bloc/bloc.dart";
import "../widget/widget.dart";
import "../model/model.dart";

class SearchScene extends StatefulWidget{
  final String remark;
  final String path;

  SearchScene({Key key, this.remark, this.path}):super(key: key);

  @override
    State<StatefulWidget> createState() {
      return new _SearchState();
    }
}

class _SearchState extends State<SearchScene>{
  final _bloc = new StuffsBloc();
  String _q = "";

  void _fetchStuffs(int p) {
    return _bloc
        .add(FetchStuffs(p: p, q: _q, remark: widget.remark, path: widget.path));
  }

  void _refreshStuffs() {
    return _bloc.add(RefreshStuffs(q: _q, remark: widget.remark, path: widget.path));
  }

  Widget _renderInput(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10.0),
      child: Theme(
        data: Theme.of(context).copyWith(
          hintColor: Colors.grey,
          primaryColor: Colors.black12
        ),
        child: TextField(
          textInputAction: TextInputAction.search,
          onSubmitted: (String val) {
            setState(() {
              _q = val;
            });
            _fetchStuffs(1);
          },
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0),
            border: OutlineInputBorder(),
            hintText: '搜索资源',
            hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey)
          ),
        )
      )
    );
  }

  Widget _renderRow(BuildContext context, List data, int i) {
    final _stuff = data[i] as Stuff;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 12.0),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.black12))
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(_stuff.name),
                RichText(
                  text: TextSpan(
                    text: _stuff.updatedAt,
                    style: TextStyle(color: Colors.black45, fontSize: 12.0),
                    children: <TextSpan>[
                      TextSpan(text: '   ' + _stuff.remark),
                      TextSpan(text: '/' + _stuff.path)
                    ]
                  )
                )
              ],
            ),
          )
        ],
      )
    );
  }

  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.5,
          centerTitle: true,
          title: _renderInput(context),
          leading: null,
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            MaterialButton(
              minWidth: 60.0,
              child: Text('取消', style: TextStyle(color: Colors.grey)), 
              padding: const EdgeInsets.all(0.0),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ],
        ),
        body: SafeArea(
          bottom: true,
          child: _q == '' ? Container() : PRList(
            bloc: _bloc,
            fetchRows: _fetchStuffs,
            refresh: _refreshStuffs,
            renderRow: _renderRow,
          )
        ),
      );
    }
}