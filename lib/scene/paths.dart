import "package:flutter/material.dart";
import 'package:flutter_bloc/flutter_bloc.dart';
import "../bloc/bloc.dart";
import "../widget/widget.dart";
import "./stuffs.dart";
import "./search.dart";

class PathScene extends StatefulWidget {
  final String remark;

  PathScene({Key key, @required this.remark}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _PathState();
  }
}

class _PathState extends State<PathScene> {
  final _bloc = new PathsBloc();

  @override
  void initState() {
    super.initState();
    _bloc.add(FetchPaths(remark: widget.remark));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.5,
          centerTitle: true,
          title: Text("Path of ${widget.remark}",
              style: TextStyle(fontSize: 16.0)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            iconSize: 16.0,
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.search),
                iconSize: 20.0,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new SearchScene(remark: widget.remark)));
                })
          ]),
      body: SafeArea(
          bottom: true,
          child: BlocBuilder<PathsBloc, PathsState>(
            bloc: _bloc,
            builder: (BuildContext context, PathsState state) {
              if (state is PathsLoaded) {
                return ListView.separated(
                  itemCount: state.paths.length,
                  separatorBuilder: (BuildContext context, int i) => Divider(),
                  itemBuilder: (BuildContext context, int i) {
                    final _path = state.paths[i];
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => new StuffsScene(
                                  remark: widget.remark,
                                  path: _path,
                                )));
                      },
                      title: Text(_path,
                          style:
                              TextStyle(color: Colors.black87, fontSize: 14.0)),
                    );
                  },
                );
              }
              if (state is PathsError) {
                return RequestFail(
                    refresh: () => _bloc.add(FetchPaths(remark: widget.remark)));
              }
              return Center(
                  child: CircularProgressIndicator(
                strokeWidth: 2.0,
              ));
            },
          )),
    );
  }
}
