import "package:flutter/material.dart";
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/bloc.dart';
import "../widget/widget.dart";
import "./paths.dart";

class HomeScene extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HomeState();
  }
}

class _HomeState extends State<HomeScene> {
  final _bloc = new RemarksBloc();

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void reassemble() {
    super.reassemble();
    _init();
  }

  void _init() {
    _bloc.add(RemarksEvent.Fetch);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        title: Text("Remark", style: TextStyle(fontSize: 16.0)),
      ),
      body: SafeArea(
        bottom: true,
        child: BlocBuilder<RemarksBloc, RemarksState>(
          bloc: _bloc,
          builder: (BuildContext context, RemarksState state) {
            if (state is RemarksLoaded) {
              return GridView.count(
                crossAxisCount: 3,
                children: state.remarks.map<Widget>((v) {
                  return InkResponse(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new PathScene(remark: v)));
                      },
                      child: Card(
                        child: Center(
                            child: Text(v,
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black87))),
                      ));
                }).toList(),
              );
            }
            if (state is RemarksError) {
              return RequestFail(refresh: _init);
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
