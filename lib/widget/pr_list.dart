part of widget;

class PRList extends StatefulWidget {
  final Function(int) fetchRows;
  final VoidCallback refresh;
  final Widget Function(BuildContext, List, int) renderRow;
  final Bloc bloc;

  PRList(
      {Key key,
      @required this.refresh,
      @required this.fetchRows,
      @required this.renderRow,
      @required this.bloc})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _PRListState();
  }
}

class _PRListState extends State<PRList> {
  ScrollController _scrollController = new ScrollController();
  int _page = 1;
  bool _isLoadingMore = false;
  bool _isDone = false;
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_loadMore);
  }

  void _loadMore() {
    if (_scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent) {
      if (_isLoadingMore) return;
      if (_isDone) return;
      setState(() {
        _isLoadingMore = true;
      });
      widget.fetchRows(_page);
      print("load more");
    }
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_loadMore);
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
        bloc: widget.bloc,
        listener: (context, state) {
          if (state is StuffsLoaded || state is StuffsRefreshed) {
            PRData _data = state.data;
            setState(() {
              _isLoadingMore = false;
            });
            if (_data.total < 10) {
              setState(() {
                _isVisible = false;
              });
            }
            if (_data.total == _data.count) {
              setState(() {
                _isDone = true;
              });
            }
            if (_data.total > _data.count) {
              setState(() {
                _page += 1;
                _isDone = false;
              });
            }
          }
        },
        builder: (context, state) {
          if (state is StuffsLoaded) {
            print("render ${state.data.count}");
            return _renderList(context, state.data);
          } else if (state is StuffsError) {
            return RequestFail(refresh: () {
              widget.fetchRows(1);
            });
          }
          return Center(child: CircularProgressIndicator(strokeWidth: 2.0));
        });
  }

  Widget _renderList(BuildContext context, PRData data) {
    return RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _page = 1;
          });
          widget.refresh();
        },
        child: ListView.builder(
          controller: _scrollController,
          itemCount: data.rows.length + 1,
          itemBuilder: (BuildContext context, int i) =>
              _renderRow(data.rows, i, data.total),
        ));
  }

  Widget _renderRow(List rows, int i, int count) {
    if (i == rows.length) {
      return LoadMore(
          activated: _isLoadingMore, done: _isDone, visible: _isVisible);
    }
    return widget.renderRow(context, rows, i);
  }
}
