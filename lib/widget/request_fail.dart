part of widget;

class RequestFail extends StatelessWidget {
  final VoidCallback refresh;

  RequestFail({Key key, @required this.refresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Icon(Icons.error_outline, color: Colors.grey, size: 25.0),
          SizedBox(height: 8.0),
          Text("请求异常", style: TextStyle(color: Colors.grey, fontSize: 14.0)),
          SizedBox(height: 8.0),
          FlatButton(child: Text("重试"), onPressed: refresh)
        ]));
  }
}
