part of widget;

class LoadMore extends StatelessWidget {
  final bool activated;
  final bool done;
  final bool visible;

  LoadMore(
      {Key key,
      @required this.activated,
      @required this.done,
      @required this.visible})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!visible) {
      return Container(width: 0.0, height: 0.0);
    }
    if (done) {
      return _Idle(hintText: "已加载全部");
    }
    return activated ? _Loading() : _Idle(hintText: "上拉加载更多");
  }
}

class _Idle extends StatelessWidget {
  final String hintText;

  _Idle({Key key, @required this.hintText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(hintText,
            style: TextStyle(color: Colors.grey, fontSize: 12.0)));
  }
}

class _Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CupertinoActivityIndicator(),
            SizedBox(width: 12.0),
            Text("加载中", style: TextStyle(color: Colors.grey, fontSize: 12.0))
          ],
        ));
  }
}
