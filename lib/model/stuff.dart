part of model;

class Stuff{
  final String name;
  final String remark;
  final String path;
  final String updatedAt;

  Stuff({
    this.name, this.remark, this.path, this.updatedAt
  });

  Stuff.fromJson(Map json):
    this.name = json["Name"],
    this.remark = json["Remark"],
    this.path = json["Path"],
    this.updatedAt = fromNow(json["UpdatedAt"]);
}