part of repository;

Future<List<String>> fetchRemarks() async {
  final response = await http.get(apis["remarks"]);
  if (response.statusCode != 200) {
    throw Exception("failed to gettting remarks");
  }
  final ret = jsonDecode(response.body);
  return ret["remarks"].cast<String>();
}
