library repository;

import 'dart:convert';
import "package:http/http.dart" as http;

part "remark.dart";
part "paths.dart";
part "stuffs.dart";

const apiHost = "https://play.alilab.org/lok/";

const apis = <String, String>{
  "remarks": "$apiHost/remarks",
  "paths": "$apiHost/paths",
  "search": "$apiHost/search",
};
