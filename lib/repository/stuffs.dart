part of repository;

Future<dynamic> fetchStuffs(
    {int p = 1, String q = "", String remark, String path}) async {
  String _url = "${apis['search']}?page=$p&limit=15";
  if (q != "") {
    _url += "&key=$q";
  }
  if (remark != null) {
    _url += "&remark=$remark";
  }
  if (path != null) {
    _url += "&path=$path";
  }
  print("url ==> $_url");
  final response = await http.get(_url);
  if (response.statusCode != 200) {
    throw Exception("failed to gettting remarks");
  }
  return jsonDecode(response.body);
}
