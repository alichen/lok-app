part of repository;

Future<List<String>> fetchPaths(String remark) async {
  final response = await http.get('${apis["paths"]}?remark=$remark');
  if (response.statusCode != 200) {
    throw Exception("failed to gettting paths");
  }
  final ret = jsonDecode(response.body);
  return ret["paths"].cast<String>();
}
